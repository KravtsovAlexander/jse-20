package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Bind task to project.";

    public static final String NAME = "task-bind-to-project";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskID = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(getUserId(), projectID, taskID);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
