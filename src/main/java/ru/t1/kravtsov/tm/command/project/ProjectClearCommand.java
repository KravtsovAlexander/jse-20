package ru.t1.kravtsov.tm.command.project;

import ru.t1.kravtsov.tm.model.Project;

import java.util.List;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove all projects.";

    public static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        final List<Project> projects = getProjectService().findAll();
        getProjectTaskService().removeProjects(getUserId(), projects);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
