package ru.t1.kravtsov.tm.command.system;

import ru.t1.kravtsov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display developer info.";

    public static final String NAME = "about";

    @Override
    public void execute() {
        final IPropertyService propertyService = getPropertyService();
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + propertyService.getAuthorName());
        System.out.println("EMAIL: " + propertyService.getAuthorEmail());
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
