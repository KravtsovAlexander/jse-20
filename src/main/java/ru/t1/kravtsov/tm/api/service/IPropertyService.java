package ru.t1.kravtsov.tm.api.service;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

}
