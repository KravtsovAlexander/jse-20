package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjects(String userId, List<Project> projects);

}
